//directory that contains files that is used to build application.
//reverse domain name notation (Ex. com.android or com.zuitt)

package com.zuitt;

import java.util.Locale;

// "Main" is the Entry point of a java program and is responsible for executing our code
public class Main {
    public static void main(String []args){
        System.out.println("hello world");
        System.out.println("SCRIPS");

        int f;

        f =340;
        System.out.println(f);

        //constants
        final int principal =2323;

        /*
        [Section] Primitive Data Type
        -use to store simple values
         */
        //single quotes(char)
        char letter = 'a';
        boolean isMarried = false;
        //byte is used to limit or set size for memory allocation

        //-128 to 127
        byte students = 127;

        //-32768 to 32767
        short seat = 32767;

        //Underscores may be placed in between number for code readability
        int localPopulation = 1_223_123_123;

        long worldPopulation  = 7_862_081_145L;

        //[Section] Float and Doubles
        // The difference between use float and doubles  on the preciseness of
        float price = 12.99F;
        double temperature = 15869.8623941;

        //checking the data type of identifier /variable
        System.out.println("Result og getClass Method: ");

        System.out.println(((Object)temperature).getClass());

        //[Section] Non-primitive
        String name = "John Doe";
        System.out.println("Result of Non-primitive Data Type: ");
        System.out.println(name);
        String editName = name.toLowerCase();
        System.out.println(editName);

        //[Section] Type Casting

        //Implicit Casting
        int num1 = 5;
        double num2 = 2.7;
        double total = num1 + num2;
        System.out.println("Result from implicit casting: ");
        System.out.println(total);

        // Explicit Casting
        int num3 = 5;
        double num4 = 2.7;
        int anotherTotal = num3 + (int)num4;

        System.out.println("Result from Explicit casting: ");
        System.out.println(anotherTotal);
    }
}
